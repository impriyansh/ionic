import { Component, OnInit, Inject } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Dish } from '../../shared/dish';
import { DishProvider } from '../../providers/dish/dish';
import { Promotion } from '../../shared/promotion';
import { PromotionProvider } from '../../providers/promotion/promotion';
import { Leader } from '../../shared/leader';
import { LeaderProvider } from '../../providers/leader/leader';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  dish: Dish;
  dishErrMess: string;
  promotion: Promotion;
  promoErrMess: string;
  leader: Leader;
  leaderErrMess: string;

  constructor(public navCtrl: NavController,
    private dishService: DishProvider,
    private promotionService: PromotionProvider,
    private leaderService: LeaderProvider,
    @Inject('BaseURL') public BaseURL) { }

  ngOnInit() {
    this.dishService.getFeaturedDish()
      .subscribe(dish => this.dish = dish,
      err => this.dishErrMess = <any>err);
    this.promotionService.getFeaturedPromotion()
      .subscribe(promotion => this.promotion = promotion,
      err => this.promoErrMess = <any>err);
    this.leaderService.getFeaturedLeader()
      .subscribe(leader => this.leader = leader,
      err => this.leaderErrMess = <any>err);
  }
}
